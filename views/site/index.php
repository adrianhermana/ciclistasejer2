<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">   
    <h1>Ejercicio 2 consultas</h1> 
    <p>Ejercicios del 1 al 12</p> 
    </div>

    
    <div class="body-content">

        <div class="row">
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 1</h2>
                <p>Número de ciclistas que hay</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord1'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao1'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 2</h2>
                <p>Número de ciclistas que hay del equipo Banesto</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord2'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao2'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 3</h2>
                <p>Edad media de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord3'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao3'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 4</h2>
                <p>La edad media de los del equipo Banesto</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord4'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao4'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 5</h2>
                <p>La edad media de los ciclistas por cada equipo</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord5'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao5'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 6</h2>
                <p>El número de ciclistas por equipo</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord6'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao6'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 7</h2>
                <p>El número total de puertos</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord7'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao7'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 8</h2>
                <p>El número total de puertos mayores de 1500</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord8'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao8'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 9</h2>
                <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord9'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao9'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 10</h2>
                <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord10'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao10'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 11</h2>
                <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord11'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao11'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                <h2>Ejercicio 12</h2>
                <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>
                <p>
                    <?= Html::a('Active Record', ['site/activerecord12'], ['class' => 'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/dao12'], ['class' => 'btn btn-default'])?>
                </p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    </div>
</div>
